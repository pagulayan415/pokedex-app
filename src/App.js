import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Homepage from './Pages/Homepage';
import Pokemon from './Pages/Pokemon';
import { PokemonProvider } from './PokemonContext';

const App = () =>{
  
return (
  <PokemonProvider>
    <Router>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/status" element={<Pokemon />} />
      </Routes>
    </Router>
  </PokemonProvider>
  );
}

export default App;
