import React, { useContext } from "react";
import PokemonStatus from "../Components/PokemonStatus";
import { PokemonContext  } from "../PokemonContext";

const Pokemon = () =>{
    const [allPokemons] = useContext(PokemonContext);
    // console.log(allPokemons)
    return(
        <div>
            {allPokemons.map((pokemon, index) =>(
                <PokemonStatus
                id={pokemon.id}
                name={pokemon.name}
                image={pokemon.sprites.other.dream_world.front_default}
                key={index}
                />
            ))}
        </div>
    )
}

export default Pokemon;