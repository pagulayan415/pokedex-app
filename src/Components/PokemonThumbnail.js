import React from "react";
import {Col, Card} from "react-bootstrap";
import { Link } from 'react-router-dom';

const PokemonThumbnail = (props) =>{
    const {id, name, image} = props;
    return(
        <Link to={`/status/`} >
            <Col xs={12} className="mt-4 pokemonList">
                <Card className="card1">
                        {id}
                        <img src={image} alt={name} className="pokemonImage"></img>
                        {name}
                </Card>
            </Col>
        </Link>

    )
}

export default PokemonThumbnail;