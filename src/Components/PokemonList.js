import { useContext } from "react";
import PokemonThumbnail from "./PokemonThumbnail";
import { Button } from "react-bootstrap";
import { PokemonContext } from "./../PokemonContext";



const PokemonList = () =>{
    const [allPokemons, getAllPokemons] = useContext(PokemonContext);
    // console.log(allPokemons)


    return(
            <div className="pokemon-container">
                <div>
                    { allPokemons.map((pokemon, index) => 
                        <Button key={pokemon.id}>
                            <PokemonThumbnail
                            name={pokemon.name}
                            image={pokemon.sprites.other.dream_world.front_default}
                            key={index}
                            />
                            
                        </Button>
                    )}
                </div>
                <button className="load-more" onClick={() => getAllPokemons()}>Load More</button>
            </div>
    );
}


export default PokemonList;